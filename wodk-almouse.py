'''
Discord bot for setting availability for a Wodka game
By Zach Smith


This bot is designed to automatically interact with a
Discord server for scheduling games of a card game, Wodka.
The server uses a "looking for game" channel, in which
players can leave a message to search for a game. If enough
reactions are left on such a message, the users are pinged
and the game starts.

I don't like discord notifications, so this bot is used to
automatically post messages and/or emote, as well as notify
me if a game is found

Note that the following files must be provided:
    - .env, which contains several keys (see the last lines)

Marks (ctrl+f to find):
    - Constants
    - Setup
    - User Interface
    - Initialization
    - Called/Scheduled Tasks

Open Todos:
    - Add sound files to notify on start
    - Port UI to something raspberry-pi compatible
    - Add raspberry-pi-button-support
    - Becoming not-lfg should remove any active emotes
    - Becoming lfg should add emotes to any existing lfg games

'''

import asyncio
import time
import sys
import os
import requests
import random
import threading

import numpy as np
import time

import discord
from discord.ext import commands

from dotenv import load_dotenv

import yaml

from datetime import datetime, timedelta
from playsound import playsound

from PySide2 import QtCore, QtWidgets, QtGui

# Pip3 libraries:
# discord.py asyncio pyyaml numpy python-dotenv datetime playsound requests PySide2 threading

intents = discord.Intents.default()
intents.reactions = True
intents.members = True
client = discord.Client(intents=intents)

#-----------------------------------------------------------------------------#
#---------------------------------- Constants --------------------------------#
#-----------------------------------------------------------------------------#

LFG_GAME_CHANNEL = "looking-for-game"
NOT_LFG_STRING = "Not LFG"

LFG_EMOTE = "🔔"

GAME_FOUND_SOUNDS = ["sounds/timetoshine.mp3",
    "sounds/wodoclock.mp3"    
]

#-----------------------------------------------------------------------------#
#---------------------------------- Utility ----------------------------------#
#-----------------------------------------------------------------------------#

def is_client_lfg():
    return client.lfgUntil > datetime.now()


async def play_game_found_sound():
    playsound(random.choice(GAME_FOUND_SOUNDS)) # Second argument is play async

#-----------------------------------------------------------------------------#
#------------------------------- User Interface ------------------------------#
#-----------------------------------------------------------------------------#

class MyWidget(QtWidgets.QWidget):
    def __init__(self):
        super().__init__()

        self.hourLfgButton = QtWidgets.QPushButton("LFG 1 Hour")
        self.alwaysLfgButton = QtWidgets.QPushButton("Permanent LFG")
        self.disableLfgButton = QtWidgets.QPushButton("Disable LFG")
        self.lfgUntilLabel = QtWidgets.QLabel(NOT_LFG_STRING)
        self.lfgUntilLabel.setAlignment(QtCore.Qt.AlignCenter)

        self.layout = QtWidgets.QVBoxLayout()
        self.layout.addWidget(self.lfgUntilLabel)
        self.layout.addWidget(self.hourLfgButton)
        self.layout.addWidget(self.alwaysLfgButton)
        self.layout.addWidget(self.disableLfgButton)
        self.setLayout(self.layout)

        self.hourLfgButton.clicked.connect(self.add_onehr_lfg)
        self.alwaysLfgButton.clicked.connect(self.add_always_lfg)
        self.disableLfgButton.clicked.connect(self.disable_lfg)

    def add_onehr_lfg(self):
        self.add_time_to_lfg(timedelta(hours=1))
    def add_always_lfg(self):
        self.add_time_to_lfg(timedelta(weeks=52))

    def add_time_to_lfg(self, timedelta):
        if (client.lfgUntil < datetime.now()):
            client.lfgUntil = datetime.now()
        client.lfgUntil = client.lfgUntil + timedelta
        self.update_lfg_label()

    def disable_lfg(self):
        client.lfgUntil = datetime.now()
        self.update_lfg_label()

    def update_lfg_label(self):
        if not is_client_lfg():
            self.lfgUntilLabel.setText(NOT_LFG_STRING)
        else:
            lfgUntilTime = client.lfgUntil.strftime("%m/%d/%Y, %H:%M:%S")
            self.lfgUntilLabel.setText(f"LFG Until\n {lfgUntilTime}")



#-----------------------------------------------------------------------------#
#-------------------------- Called/Scheduled Tasks ---------------------------#
#-----------------------------------------------------------------------------#


@client.event
async def on_message(message):
    # Case 1: It's a "looking for game" post and I'm lfg
    if ((message.author != client.joeyBot)
            and (message.author != client.user)
            and (message.author != client.owner)
            and (message.channel == client.lfgChannel)
            and (is_client_lfg())):
        await message.add_reaction(LFG_EMOTE)
        return

    # Case 2: This bot has been mentioned by Joey Bot
    if ((message.author == client.joeyBot) 
            and ((client.user in message.mentions) or (client.owner in message.mentions))
            and (message.channel == client.lfgChannel)):
        print("Found a game")
        await play_game_found_sound()
        return

    # Case 3: Anything else
    else:
        return


# Scheduled task that runs every 60 seconds.
# Doesn't actually do anything
async def schedule_periodic_task():
    await client.wait_until_ready()
    await asyncio.sleep(5) # Dirty hack to make sure on_ready has actually finished running
    await periodic_task() # Initialise any variables stored in the periodic task

    # Find out how long until the next minute (e.g.) starts
    timeNow = datetime.now()
    delta = timedelta(minutes=1)
    nextMinute = (timeNow + delta).replace(second=0)
    timeToWait = (nextMinute-timeNow).seconds

    await asyncio.sleep(timeToWait)
    while True:
        await periodic_task()

        # Calculate next wait time
        timeNow = datetime.now()
        delta = timedelta(minutes=1)
        nextMinute = (timeNow + delta).replace(second=0)
        timeToWait = (nextMinute-timeNow).seconds

        # Actually wait
        await asyncio.sleep(timeToWait)

async def periodic_task():
    #await client.debugChannel.send("I am a gnome and I live in a dome")
    pass

#-----------------------------------------------------------------------------#
#---------------------------------- Setup ------------------------------------#
#-----------------------------------------------------------------------------#

# Called after the bot first connects to the server
@client.event
async def on_ready():
    for g in client.guilds:
        if g.name == GUILD:
            client.guild = g

    print(f"---- Connected to Server {client.guild.name} ----")

    # Initialize channels as variables
    for channel in client.guild.channels:
        if channel.name == LFG_GAME_CHANNEL:
            client.lfgChannel = channel

    client.joeyBot = False
    client.owner = False
    # Initialize reference to Joey bot
    for member in client.guild.members:
        if str(member.id) == JOEY_BOT_ID:
            client.joeyBot = member
            print("    Found joeyBot")
        if str(member.id) == OWNER_ID:
            client.owner = member
            print("    Found owner")

    if not client.joeyBot:
        print("Startup error: no JoeyBot")
    if not client.owner:
        print("Startup error: no Owner")

#-----------------------------------------------------------------------------#
#---------------------------- Initialization ---------------------------------#
#-----------------------------------------------------------------------------#

def initialize_ui():
    # Load up the UI
    print("---- Loading up the UI ----")
    app = QtWidgets.QApplication([])
    widget = MyWidget()
    widget.resize(800, 600)
    widget.show()
    sys.exit(app.exec_())

def client_start():
    client.run(TOKEN)


if __name__ == "__main__":
    # Get hidden environment variables (safety first, kids!)
    print("---- Wodk'Almouse Initialise ----")
    load_dotenv()
    TOKEN = os.getenv('DISCORD_TOKEN')
    GUILD = os.getenv('DISCORD_GUILD')
    JOEY_BOT_ID = os.getenv('JOEY_BOT_ID')
    OWNER_ID = os.getenv('OWNER_ID')

    if TOKEN is None:
        sys.exit("---- Tokens failed to load, do you have the .env file? ----")
    client.lfgUntil = datetime.now()

    # This adds an asynchronous task to spam the messages of the day
    # I still don't actually understand how it works, but okay.
    client.loop.create_task(schedule_periodic_task())
    # Actually kick off the bot
    print("---- Firing up the Bot ----")
    threading.Thread(target=client_start).start() 
    asyncio.get_event_loop().create_task(initialize_ui())